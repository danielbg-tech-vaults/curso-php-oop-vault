# Práctica 5: Cómo incorporar librerías de terceros

En esta práctica vamos a crear un nuevo proyecto y vamos a incorporar la librería Dotenv para cargar archivos de configuración.

## Creación del proyecto

En el directorio htdocs de tu instalación de XAMPP crea un nuevo subdirectorio llamado "ConfigApp" y cámbiate al directorio creado.

```shell
cd c:\xampp\htdocs

mkdir ConfigApp

cd ConfigApp
```

Crea un nuevo proyecto llamado ConfigApp usando Composer. En el nombre del vendor inventa tu propio nombre de vendor.

```shell
composer init --name ConfigApp vendor/config-app
```

## Instalar una librería de terceros

En la comunidad del lenguaje PHP, las librerías de terceros están catalogadas en el sitio web Packagist (https://packagist.org/) y pueden ser instaladas a través de Composer. En este sitio también es posible encontrar algo de la documentación necesaria para aprender a usar cada librería, pero en general la documentación a profundidad de cada proyecto la encontrarás en el sitio web de cada proyecto.

En esta práctica vamos a usar la librería **DotEnv** y su página de Packagist está en: https://packagist.org/packages/vlucas/phpdotenv

![[Pasted image 20221112214329.png|700]]

Para instalarla, dirígete al directorio de tu aplicación y ejecuta el siguiente comando:

```php
composer require vlucas/phpdotenv
```

Los archivos, así como otras librerías que se necesiten quedarán instalados en el directorio **vendor** del proyecto. 

La librería **DotEnv** permite cargar variables de ambiente desde un archivo llamado **`.env`** y esto es muy útil para concentrar variables "globales" que usen diversos módulos de un sistema, por ejemplo, los parámetros de conexión a la base de datos.

## Importación de la librería

Crea un archivo llamado **`.env`** en la raíz del proyecto con el siguiente contenido. Observa que es un archivo de texto plano con variables y valores que representan parámetros ficticios del sistema.

```php
app_name="Archivos de Configuración"

# Parametros de conexion a la base de datos
db_name=test
db_user=test
db_password=test
db_address=127.0.0.1
db_port=3306
```

Crea el archivo `index.php` en la raíz del proyecto e importa el cargador automático

```php
<?php

require(__DIR__ . '/vendor/autoload.php');

?>
```

A continuación, importa la librería tal como lo indica su documentación.

```php
<?php

require(__DIR__ . '/vendor/autoload.php');

use Dotenv\Dotenv;

?>
```

El siguiente paso es leer el archivo de configuración del directorio raíz. La documentación de la librería indica que una forma de lograr esto es con las siguientes instrucciones.

```php
<?php

require(__DIR__ . '/vendor/autoload.php');

use Dotenv\Dotenv;

// Abre el archivo y carga las variables.
$dotEnv = Dotenv::createImmutable(__DIR__);

$dotEnv->safeLoad();
?>
```

Las variables leídas del archivo quedarán disponibles como elementos del arreglo **`$_ENV`** y podemos leerlas desde ahí.

```php
<?php

require(__DIR__ . '/vendor/autoload.php');

use Dotenv\Dotenv;

// Abre el archivo y carga las variables.
$dotEnv = Dotenv::createImmutable(__DIR__);

$dotEnv->safeLoad();

// Lee una variable. Podemos usar el operador '??' para asignar un valor por default cuando la variable no traiga un valor.
$title = $_ENV['app_name'] ?? "App sin nombre";
?>
```

Finalmente, imprimiremos los parámetros usando un volcado simple con la instrucción `print_r()`
```php
<?php

require(__DIR__ . '/vendor/autoload.php');

use Dotenv\Dotenv;

// Abre el archivo y carga las variables.
$dotEnv = Dotenv::createImmutable(__DIR__);

$dotEnv->safeLoad();

// Lee una variable. Podemos usar el operador '??' para asignar un valor por default cuando la variable no traiga un valor.
$title = $_ENV['app_name'] ?? "App sin nombre";
?>

<head>
    <title><?php echo $title; ?></title>
</head>

<h1>Cargar parametros de configuraci&oacute;n</h1>

Parametros de configuraci&oacute;n de la aplicaci&oacute;n:
<pre>
    <?php print_r($_ENV); ?>
</pre>
```

La salida de este script se muestra a continuación.

![[Pasted image 20221112220346.png|700]]

A continuación, incorpora el archivo **`.gitignore`** a este proyecto también, como lo hicimos en el proyecto "Hola, Mundo!" porque lo vas a subir a un repositorio en tu Gitlab. 

## No se versiona el archivo `.env`

Esto es algo que pudiéramos dar por obvio pero es mejor remarcarlo. Los archivos de configuración como el .env que acabamos de crear no deben subirse al repositorio Gitlab porque esto supone un riesgo de seguridad para nuestro sistema ya que, cualquiera con acceso al repositorio podría conocer nuestras credenciales de conexión a la base de datos u otro tipo de información que debiera ser privada.

Sin embargo, siempre es necesario versionar una copia del archivo **.env** que sirva de plantilla para configurar las variables requeridas por el sistema para reinstalarlo cuando sea necesario. Podemos crear una copia de nuestro archivo **.env** y modificar o borrar, los valores reales de las variables y versionar esta copia que será la plantilla.

```shell
cp .env .env.example
```

Un ejemplo de cómo quedaría el contenido del archivo **.env.example** es el siguiente.

```php
app_name="Archivos de Configuración"

# Parametros de conexion a la base de datos
db_name=EL_NOMBRE_DE_TU_BD_SIN_COMILLAS
db_user=TU_LOGIN_SIN_COMILLAS
db_password=Tu_Password_sin_comillas
db_address=MySQL_SERVER_IP
db_port=PUERTO_MYSQL
```

En el archivo **.gitignore** existe una línea para ignorar el archivo .env que te servirá para evitar subir el archivo al repositorio.

## Ejercicios

01. Para practicar, utiliza el ciclo foreach de PHP y una tabla de HTML para formatear la salida y presentar los parámetros de configuración en forma de tabla.

02. Crea un nuevo repositorio bajo el grupo de tus proyectos del curso para este nuevo proyecto y sube el código que acabas de crear.
