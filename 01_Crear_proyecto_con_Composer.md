# Práctica 1: Crear un proyecto con Composer

En esta práctica vamos a crear un proyecto de PHP utilizando el gestor de dependencias Composer.

## Creación del proyecto

El primer paso es abrir la línea de comandos y dirígete a tu carpeta htdocs de XAMPP. 

**NOTA:** Toma en cuenta que la ruta de tu instalación podría variar respecto a la presentada en esta práctica. XAMPP normalmente se instala en la ruta `c:\xampp\` pero acostumbro cambiar su nombre durante la instalación para diferenciar la versión de PHP que incluye porque a veces necesito trabajar con versiones anteriores del lenguaje.

```shell
cd c:\xampp-8.1.10\htdocs
```

![[Pasted image 20221112142407.png]]

A continuación, crea un directorio para el proyecto nuevo y muévete al interior del mismo.

```shell
mkdir HolaMundo

cd HolaMundo
```

![[Pasted image 20221112142847.png]]

**Usa el comando `composer init --name vendor-name/nombre-del-proyecto` para crear un proyecto de PHP**

- El *"vendor-name"* lo definimos arbitrariamente para indicar quién será el proveedor del proyecto. Por ejemplo, puedes usar tu nombre de usuario `composer init --name danielbg/hola-mundo` 
- El *"nombre-del-proyecto"* se compone de letras minúsculas y sin espacios, puede llevar algunos caracteres especiales. Es recomendable que sea un nombre simple y relacionado con el propósito del proyecto.

Crea tu proyecto "Hola, Mundo", definiendo tu propio nombre de vendor. El parámetro `-d` sirve para indicar el nombre del directorio que usará para crear los archivos y subdirectorios del proyecto, en este caso uso el punto porque quiero que los archivos sean creados en el directorio actual, pero es posible indicar rutas.

```shell
# Ejemplo:
composer init --name danielbg/hola-mundo -d .
```

Esto iniciará el proceso interactivo para generar un proyecto con Composer y durante el mismo te pedirán los valores para diversos parámetros. Usa la siguiente tabla como referencia para dicho proceso.

| Parámetro | Valor o acción |
| - | - |
| Package Name | Pulsa ENTER para aceptar el valor que sugerido o cambialo si lo deseas |
| Description | Pulsa ENTER o ingresa algo como "Mi primer proyecto con Composer" |
| Author | Pulsa ENTER para aceptar el valor que muestra o ingresa tu nombre completo |
| Minimum stability | Pulsa ENTER o ingresa el valor dev. TIP: Puedes ingresar un valor incorrecto como 1.0 para que veas las opciones que Composer considera correctas |
| Package type | Ingresa la cadena "project" sin comillas, claro. |
| License | Pulsa ENTER para continuar. Opcionalmente puedes investigar sobre distintos tipos de licencias legales para código abierto como Apache, MIT, FreeBSD y muchas más |
| Would you like to define your dependencies? | Ingresa "no" (sin comillas) y pulsa ENTER | 
| Would you like to define your dev dependencies? | Ingresa "no" (sin comillas) y pulsa ENTER | 
| Add PSR-4 autoload mapping? | Pulsa ENTER para aceptar los valores sugeridos |
| Do you confirm generation? | Pulsa ENTER para generar el archivo JSON con los valores configurados por el proceso |

La pantalla final del proceso debe ser similar a la siguiente imagen

![[Pasted image 20221112145704.png|800]]
![[Pasted image 20221112145813.png|800]]

Observa que se han creado los siguientes elementos dentro del directorio del proyecto:  
- El archivo **composer.json**.
- Los subdirectorios **src** y **vendor**.

![[Pasted image 20221112151942.png]]

- Las librerías de terceros se guardarán en el subdirectorio **vendor**.
- La configuración de composer para este proyecto se encuentra en el archivo **composer.json**.
- Las clases o librerías de nuestra propia autoría se colocarán en el directorio **src**

### Creación del script `index.php`

A continuación, vamos a crear el script **index.php** sobre la raíz del proyecto para que sea el punto de entrada. Abre el directorio del proyecto desde Visual Studio Code  para crear el archivo index.php sobre el directorio raíz del proyecto con el siguiente código

```php
<h1>Hola, Mundo!</h1>

<?php phpinfo(); ?>
```

![[Pasted image 20221112153945.png|800]]

### Ejecución del proyecto

Vamos a probar el proyecto usando el servidor Apache que instalamos con XAMPP. Abre el panel de control de XAMPP e inicia el servidor Apache para visualizar la página en un navegador.

![[Pasted image 20221112152726.png|600]]

En tu navegador ingresa al URL : **http://127.0.0.1/HolaMundo/** y se deberá mostrar el mensaje que ingresamos en nuestro **index.php** junto con la salida de la función **phpinfo()** como se muestra en la siguiente imagen:

![[Pasted image 20221112154026.png|800]]

