
# Práctica 4: Creación de un grupo en Gitlab para subir los proyectos del curso

En esta práctica vamos a crear un grupo público en Gitlab para subir los proyectos realizados en el curso. Primero ingresa a Gitlab con tu cuenta de usuario y dirígete al URL https://gitlab.com/dashboard/groups donde podrás ver el listado de tus grupos.

![[Pasted image 20221113122750.png|700]]

También puedes llegar por las opciones del menú de "hamburguesa" (el que está formado por 3 líneas apiladas, así se le conoce) bajo la sección **"Switch to"** haz clic en **"Groups"** y selecciona **"View all Groups"** para ir al mismo dashboard de tus grupos de Gitlab.

![[Pasted image 20221113134100.png|700]]

Ahora, pulsa el botón **"New group"** y, posteriormente elige la opción **"Create group"** 

![[Pasted image 20221113122918.png|700]]

- En el formulario de creación, ingresa un nombre pertinente para el grupo.
- Elige la **visibilidad pública** para tu grupo.
- Elige el role "**Software Developer**".
- Elige "**Just me**" en la pregunta de quién usará este grupo.
- Elige "**I want to store my code**" en la pregunta de para qué usarás este grupo.
- Pulsa el botón "**Create group**" para finalizar la creación.

## Subir el proyecto "Hola, mundo"
Ahora vamos a subir el proyecto que trabajamos en prácticas anteriores llamado "Hola, Mundo!"

Primero, dirígete al grupo de proyectos que acabas de crear y pulsa el botón "New project"

![[Pasted image 20221113123844.png|700]]

A continuación elige la opción "Create blank project"

![[Pasted image 20221113123951.png|700]]


- En el formulario de creación del proyecto elige un nombre pertinente para el proyecto.
- En el campo **Project slug** ingresa un alias del nombre, simplificado. Por ejemplo:
![[Pasted image 20221113124222.png]]
- En **project deployment** elige "No deployment planned"
- Elige **visibilidad pública** para el proyecto
- Desmarca la opción **"Initialize repository with a README"** porque lo queremos vacío.
- Pulsa el botón **"Create project"** para finalizar la creación

![[Pasted image 20221113124520.png|700]]
A continuación, sigue las instrucciones que sugiere Gitlab para configurar tu cuenta de acceso de Gitlab (las de `git config` para establecer el username y el email) y para subir un directorio existente (**Push an existing folder**).

![[Pasted image 20221113124723.png|700]]

Una vez que hayas subido tu código observa tu repositorio en Gitlab y notarás que, gracias al archivo **`.gitignore`** no se subirá el archivo **`composer.lock`** ni el subdirectorio **`vendor`** Esto es correcto. 

El archivo **`composer.lock`** guarda las versiones de las librerías que instales en tu proyecto y el directorio **`vendor`** guarda las librerías de terceros y no se incluyen en el repositorio del proyecto porque pueden obteneres por medio de un proceso de instalación. Además, su código ya existe en su propio repositorio y al versionarlas solo estaríamos duplicándo código de repositorios existentes.

## Instalación de un proyecto PHP

Para probar el proceso de descarga e instalación de un proyecto de PHP crea un nuevo directorio y vamos a descargar el código del repositorio que acabamos de crear, finalmente vamos a ejecutar el comando de instalación de composer. Este es el proceso común para echar a andar un proyecto de PHP creado con Composer y que reside en un repositorio de GitLab.

### Creación del subdirectorio

El primer paso será crear un nuevo subdirectorio debajo de `c:\xampp\htdocs\`  así que el primer paso será dirígirse a dicho directorio y crear un directorio para la copia. Podemos llamarlo `copia-hola-mundo` y, una vez creado, posicionate dentro del mismo:

```shell
cd c:\xampp\htdocs\

mkdir copia-hola-mundo

cd copia-hola-mundo
```

### Clonación del repositorio

El siguiente paso será clonar tu repositorio con `git clone` . Por ejemplo, yo clonaría mi repositorio de la siguiente manera:
```shell
git clone https://gitlab.com/curso-objetos-php/hola-mundo-php-objetos.git .
```

La salida del comando será algo similar a la siguiente imagen.

![[Pasted image 20221113133207.png|700]]

Al listar los archivos observa que no tienes una carpeta vendor ni un archivo composer.lock como en tu directorio original donde empezaste la creación del proyecto.

![[Pasted image 20221113133306.png]]

### Instalación

Ejecuta el siguiente comando para instalar el proyecto:

```shell
composer install
```

El gestor de dependencias Composer leerá la configuración del archivo composer.json para reconstruir el proyecto, crea un subdirectorio vendor y descarga ahí las dependencias del proyecto. 

De esta forma podras versionar y retomar tus proyectos de PHP para trabajarlos en diferentes equipos de cómputo.

