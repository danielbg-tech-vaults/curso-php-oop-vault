
# Práctica 6: Creación de la base de código para el proyecto del sistema de ventas

En esta práctica vamos dar los primeros pasos para crear un sistema que realizará las funciones básicas de un sistema de ventas para un comercio local. 

## Inicialización del proyecto con Composer

Primero vamos a crear el subdirectorio `c:\xampp\htdocs\sistema-ventas`  
```shell
mkdir c:\xampp\htdocs\sistema-ventas
```

Ahora vamos a inicializar el proyecto con Composer como ya lo hemos hecho en prácticas anteriores. Recuerda que debes especificar tu propio nombre de "vendor".
```shell
cd c:\xampp\htdocs\sistema-ventas

composer init --name vendor/sistema-ventas -d .
```

![[Pasted image 20221113141502.png]]

## Configuración del namespace raíz del proyecto

Abre el proyecto en Visual Studio Code y modifica el espacio de nombres de raíz en el archivo composer.json. 
![[Pasted image 20221113154140.png]]

Vuelve a recargar la configuración con el comando `composer dump-autoload`
```shell
composer dump-autoload
```

A continuación, crea un archivo llamado `index.php` en la raíz del proyecto y en este, agrega la importación al archivo `autoload.php` y un título para el sistema de ventas. Al visualizarlo con el navegador, se debe ver como muesta la siguiente pantalla:
![[Pasted image 20221113154852.png|500]]

## Probando la librería Monolog

## Instalación de la librería

Incorpora algunas librerías con el comando `composer require`  como lo hemos realizado en proyectos anteriores. Las librerías que se van a incorporar son las siguientes:
- monolog/monolog

Vamos a probar la librería Monolog. Esta permite enviar mensajes de bitácora (log) a archivos de texto que sirven para dejar huella cuando algo sale mal en el sistema y esto ayuda a depurar la funcionalidad para detectar dónde está ocurriendo un problema de nuestra aplicación. 

Primero crea un directorio en la raíz del proyecto llamado `logs` 
```shell
mkdir c:\xampp\htdocs\sistema-ventas\logs\
```

Debe quedar al mismo nivel del directotrio `src` como se muestra en la siguiente imagen:
![[Pasted image 20221113155149.png]]

En el archivo index.php agrega la importación de las librerías de la siguiente forma:
```php
// Importación de las librerías de Monolog
use Monolog\Level;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
```

En el mismo archivo index.php podemos probar el envío de mensajes a la bitácora con el siguiente código:
```php
// Creamos y configuramos el logger
$log = new Logger('VentasLogger');
$log->pushHandler(new StreamHandler(__DIR__ . '/logs/application.log', Level::Debug));
$log->pushHandler(new FirePHPHandler());

// Enviamos un mensaje de prueba...
$log->debug('Este mensaje se va al log');
```

En este código, creamos un objeto logger y configuramos un manejador que enviará los mensajes de bitácora al archivo que hemos definido como `application.log`

Ahora recarga la página desde el navegador, observa que la salida no debe cambiar pero, si volvemos a Visual Studio Code notarás que se ha creado el archivo `application.log` debajo del directorio `logs`
![[Pasted image 20221113161215.png]]

En el interior del archivo, deben estar los mensajes que hayamos enviado con el logger:
![[Pasted image 20221113161549.png]]


## Incorpora el archivo `.gitignore`

Descarga y copia el archivo gitignore a la raíz del proyecto como se ha hecho en las prácticas anteriores.

## Crea un repositorio para el proyecto

Ingresa a Gitlab y dirígete a tu grupo de proyectos del curso de PHP, ahí crea un nuevo repositorio para el proyecto y asignale un nombre significativo, la visibilidad del grupo debe ser pública, desmarca la opción para crear un archivo README.md.

![[Pasted image 20221113164026.png|1150]]

El proyecto debe quedar dentro del grupo de proyectos del curso.

![[Pasted image 20221113164142.png]]

A continuación, configura tus credenciales y sigue las instrucciones para subir un directorio existente al repositorio. Sube el contenido de tu directorio del proyecto como se realizó en una práctica anterior.

El repositorio remoto debe quedar de forma similar a la siguiente imagen:
![[Pasted image 20221113164556.png|800]]


