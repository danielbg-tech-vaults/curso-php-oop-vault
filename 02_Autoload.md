# Práctica 02: Carga automática de clases

Los proyectos de PHP creados con Composer incorporan un mecanismo de carga automática de clases muy fácil de usar.

### Contenido del directorio "src"

Los archivos de clases PHP que construímos para el proyecto "HolaMundo", se guardan en el directorio **src** y utilizamos los espacios de nombres de PHP, junto con el mecanismo de carga automática de clases para incorporar la funcionalidad de dichas clases.

### Espacios de nombres

Los espacios de nombres (también conocidos como *namespaces*) de PHP, permiten agrupar clases relacionadas. Podemos tener varios espacios de nombres, por ejemplo, uno para clases relacionadas con funciones de apoyo al procesamiento de datos, otro para un determinado flujo de procesamiento del negocio y otro para clases relacionadas con el acceso a bases de datos. Los espacios de nombre son un tipo ubicación virtual de la clase y su ubicación "física" en el sistema de archivos es independiente del nombre que se le asigne.

Para experimentar con los espacios de nombres, primero vamos a simplificar el espacio de nombres del proyecto HolaMundo. Esto se realizará dentro el archivo composer.json. 

Abre el archivo composer.json y busca el objeto con el nombre "autoload". A continuación, modifícalo para que coincida con el siguiente código:

```json
"autoload": {
    "psr-4": {
        "HolaMundo\\": "src/"
    }
},
```

De esta forma, el espacio de nombres `HolaMundo\\` quedará "mapeado" al directorio `src/` del proyecto. 

Guarda el archivo y ejecuta el siguiente comando: `composer dump-autoload`  El resultado deberá ser la confirmación de que, los archivos `autoload` se han vuelto a generar exitosamente.

![[Pasted image 20221112165516.png]]

Ahora, vamos a crear una clase llamada **Tiempo** dentro del directorio `src/` del proyecto 

![[Pasted image 20221112171425.png]]

Dentro del archivo ingresa el código mostrado a continuación:
![[Pasted image 20221112170233.png]]

Observa que en la línea 2 se declara que esta clase se aloja en el espacio de nombres llamado "HolaMundo", de esta forma lo ubicará PHP dentro de nuestro proyecto.

Ahora vamos a modificar el archivo index.php para que utilice la funcionalidad de la nueva clase. El primer paso es usar la función **`require()`** para importar el mecanismo de **`autoload`** y el segundo paso es "importar" la clase con la instrucción **`use`** para utilizar la clase dentro del index.

![[Pasted image 20221112172329.png|800]]

En el navegador podrémos visualizar el proyecto en el URL del proyecto: http://127.0.0.1/HolaMundo Se deberá mostrar la fecha actual conforme a la funcionalidad de nuestra clase.

![[Pasted image 20221112172703.png]]

De esta forma podemos crear las clases necesarias para un sistema dentro de subdirectorios contenidos en **src/** e importarlas.
