### Práctica 3: Incorporar el archivo `.gitignore` al proyecto "Hola, Mundo!"

Antes de incorporar el control de versiones al proyecto "Hola, Mundo" es necesario agregar el archivo de configuración que permite ignorar ciertos archivos y directorios que son comunes en proyectos de PHP.

Primero, descarga el archivo `.gitignore` que proporciona el siguiente repositorio https://gist.github.com/Yousha/a5514afd6cda8afba800f5af9f7115b4 y guárdalo en la raíz de tu proyecto "HolaMundo".  Para descargarlo, puedes clonar el repositorio, o también puedes descargar el archivo empaquetado en un ZIP a través del botón "Download ZIP".

![[Pasted image 20221113122214.png|800]]

Una vez descargado, copia el archivo **`.gitignore`**  al directorio raíz de tu proyecto.
